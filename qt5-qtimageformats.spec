%global qt_module qtimageformats

Name:           qt5-%{qt_module}
Version:        5.15.10
Release:        1
Summary:        Qt5 - QtImageFormats component

License:        LGPLv2 with exceptions or GPLv3 with exceptions
Url:            http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:        https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz

BuildRequires:  make
BuildRequires:  qt5-qtbase-devel >= %{version}
BuildRequires:  qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}

BuildRequires:  libmng-devel
BuildRequires:  libtiff-devel
#BuildRequires: jasper-devel
BuildRequires:  libwebp-devel >= 0.4.4

# prior -devel subpkg contained only runtime cmake bits
Obsoletes:      qt5-qtimageformats-devel < 5.4.0
Provides:       qt5-qtimageformats-devel = %{version}-%{release}

# filter plugin provides
%global __provides_exclude_from ^%{_qt5_plugindir}/.*\\.so$

%description
The core Qt Gui library by default supports reading and writing image
files of the most common file formats: PNG, JPEG, BMP, GIF and a few more,
ref. Reading and Writing Image Files. The Qt Image Formats add-on module
provides optional support for other image file formats, including:
MNG, TGA, TIFF, WBMP.

%package examples
Summary:        Programming examples for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
%description examples
%{summary}.


%prep
%autosetup -n qtimageformats-everywhere-src-%{version} -p1
rm -rv src/3rdparty

%build
%{qmake_qt5}
%make_build

%install
make install INSTALL_ROOT=%{buildroot}

%files
%license LICENSE.GPL*
%license LICENSE.LGPL*
%{_qt5_plugindir}/imageformats/libqmng.so
%{_qt5_plugindir}/imageformats/libqtga.so
%{_qt5_plugindir}/imageformats/libqtiff.so
%{_qt5_plugindir}/imageformats/libqwbmp.so
%{_qt5_plugindir}/imageformats/libqicns.so
#%{_qt5_plugindir}/imageformats/libqjp2.so
%{_qt5_plugindir}/imageformats/libqwebp.so
%{_qt5_libdir}/cmake/Qt5Gui/Qt5Gui_*Plugin.cmake

%changelog
* Mon Aug 21 2023 huayadong <huayadong@kylinos.cn> - 5.15.10-1
- update to version 5.15.10-1

* Wed Oct 13 2021 peijiankang <peijiankang@kylinos.cn> - 5.15.2-1
- update to upstream version 5.15.2

* Thu Mar 18 2021 maminjie <maminjie1@huawei.com> - 5.11.1-7
- Fix syntax error when macro is not defined

* Mon Sep 14 2020 liuweibo <liuweibo10@huawei.com> - 5.11.1-6
- Fix Source0

* Fri Feb 14 2020 lingsheng <lingsheng@huawei.com> - 5.11.1-5
- Package init
